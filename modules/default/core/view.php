<?php
class view extends viewAbstract {
	
	public function showCalendar($currentDate) {
		//return 1;
		$str = '<div><select name="birthday"><option>DD</option>';
		list($m, $d, $y) = explode("/", $currentDate);
		for($i = 1; $i<=31; $i++) {
			$str .= '<option value="'.$i.'" '.($i == $d ? 'selected' : '').'>'.$i.'</option>';
		}
		$str .= '</select><select name="birthmonth"><option>MM</option>';		
		for($i = 1; $i<=12; $i++) {
			$str .= '<option value="'.$i.'" '.($i == $m ? 'selected' : '').'>'.$i.'</option>'; 
		}	
		$str .= '</select><select name="birthyear"><option>YY</option>';		
		for($i = date('Y'); $i>=date('Y')-75; $i--) {
			$str .= '<option value="'.$i.'" '.($i == $y ? 'selected' : '').'>'.$i.'</option>'; 
		}
		$str .= "</select></div>";
		return $str;
	}
}
?>