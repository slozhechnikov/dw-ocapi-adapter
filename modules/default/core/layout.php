<?php
/**
 */
class layout extends layoutAbstract {

    protected $file_name = "main";

    protected $allBlocksName = array(
        "main"     => array("head", "search", "basket"),
        "checkout" => array("checkoutSteps")
    );
    public $blocks = array();

    protected $js = array(
        "main" => array("jquery-1.7.1.min", "main")
    );
}
