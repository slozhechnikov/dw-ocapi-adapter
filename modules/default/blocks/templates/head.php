<?php $url = urlService::get();
//echo "<pre>".print_r($this->categories, 1)."</pre>";
?>
<div class="c-list">
    <?php
        foreach($this->categories["categories"] as $category) {?>
    <div class="menu-list-category"><a class="level-0" href="<?php echo $url->getUrl("index", "search", array("cgid"=>$category["id"])) ?>"><span><?php echo $category["name"]?></span></a>
        <?php if(!empty($category["categories"])):?>
        <div class="menu-list-subcategories">
            <?php foreach($category["categories"] as $subCategory) {
                if(gettype($subCategory) != "array") continue;
            ?>
            <div class="menu-list-subcategory">
                <a class="level-1" href="<?php echo $url->getUrl("index", "search", array("cgid"=>$subCategory["id"]))?>"><span><?php echo $subCategory["name"];?></span></a>
            </div>
            <?php } ?>
        </div>
        <?php endif;?>
    </div>
    <?php }?>
</div>