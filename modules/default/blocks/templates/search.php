<?php
$url = urlService::get();
$searchfieldname = 'q'.lang::get()->getVal("search");
?>
<form method="POST" action="<?php echo $url->getUrl("index", "search")?>">
    <fieldset class="search-block">
        <input class="search-block-submit" type="submit" value="<?php echo lang::get()->getVal("searchsubmit")?>">
        <label class="search-block-label" >
            <span><?php echo lang::get()->getVal("searchlabel")?></span>
            <input class="search-block-text" id="search"
                   type="text"
                   placeholder="<?php echo lang::get()->getVal("searchdefaultvalue")?>"
                   value="<?php echo htmlspecialchars($_POST[$searchfieldname])?>"
                   name="<?php echo $searchfieldname?>">
        </label>
    </fieldset>
    <p class="search-suggestion"><?php echo lang::get()->getVal("Search By: Category...")?></p>
</form>
