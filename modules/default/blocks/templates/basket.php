<?php 
$url = urlService::get();
$lang = lang::get();
?>
<div class="basket-cart-top">
    <span class="basket-cart-title"><?php echo $lang->getVal("Your Shopping Cart")?></span>
    <?php if(1 || $this->quantity) { ?>
        <span class="basket-cart-totalitems" id="top_basket_quantity"><?php echo $this->quantity; ?> <?php echo $lang->getVal("items")?></span>
        <span class="basket-cart-totalprice">
            <span id="top_basket_total_price"> <?php echo $this->total_price; ?> </span> <span id="top_basket_currency"> <?php echo $this->currency ?> </span>
        </span>
        <a class="basket-cart-fullcartlink" href="<?php echo $url->getUrl("basket")?>"><?php echo $lang->getVal("Expand cart")?></a>
    <?php } ?>
</div>
<div class="basket-minicart">
    <a class="basket-minicart-close close" href="#">close</a>
    <h4 class="basket-minicart-title"><?php echo lang::get()->getVal('Added to you cart');?></h4>
    <div class="basket-minicart-tiles-list">
        <div class="basket-minicart-tile">
            <ins class="basket-minicart-tile-decor"></ins>
            <a class="basket-minicart-tile-img" href="/test/index/productPage/?pid=25502038"><img src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10208949.JJ0NLA0.PZ.jpg" alt="Flat Front Slim Pant"></a>
            <a class="basket-minicart-tile-name" href="/test/index/productPage/?pid=25502038">Flat Front Slim Pant</a>
            <dl class="basket-minicart-tile-properties">
                <dt class="basket-minicart-tile-property-label color-label">Color</dt>
                <dd class="basket-minicart-tile-property-value color-value">Laurel</dd>

                <dt class="basket-minicart-tile-property-label size-label">Size</dt>
                <dd class="basket-minicart-tile-property-value size-value">12</dd>

                <dt class="basket-minicart-tile-property-label price-label">Price</dt>
                <dd class="basket-minicart-tile-property-value price-value">73.99 USD</dd>

                <dt class="basket-minicart-tile-property-label quantity-label">Quantity</dt>
                <dd class="basket-minicart-tile-property-value quantity-value">2</dd>

                <dd class="basket-minicart-tile-price total">147.98 USD</dd>
            </dl>
        </div>
        <div class="basket-minicart-tile">
            <ins class="basket-minicart-tile-decor"></ins>
            <a class="basket-minicart-tile-img" href="/test/index/productPage/?pid=25502038"><img src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10208949.JJ0NLA0.PZ.jpg" alt="Flat Front Slim Pant"></a>
            <a class="basket-minicart-tile-name" href="/test/index/productPage/?pid=25502038">Flat Front Slim Pant</a>
            <dl class="basket-minicart-tile-properties">
                <dt class="basket-minicart-tile-property-label color-label">Color</dt>
                <dd class="basket-minicart-tile-property-value color-value">Laurel</dd>

                <dt class="basket-minicart-tile-property-label size-label">Size</dt>
                <dd class="basket-minicart-tile-property-value size-value">12</dd>

                <dt class="basket-minicart-tile-property-label price-label">Price</dt>
                <dd class="basket-minicart-tile-property-value price-value">73.99 USD</dd>

                <dt class="basket-minicart-tile-property-label quantity-label">Quantity</dt>
                <dd class="basket-minicart-tile-property-value quantity-value">2</dd>

                <dd class="basket-minicart-tile-price total">147.98 USD</dd>
            </dl>
        </div>
        <div class="basket-minicart-tile">
            <ins class="basket-minicart-tile-decor"></ins>
            <a class="basket-minicart-tile-img" href="/test/index/productPage/?pid=25502038"><img src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10208949.JJ0NLA0.PZ.jpg" alt="Flat Front Slim Pant"></a>
            <a class="basket-minicart-tile-name" href="/test/index/productPage/?pid=25502038">Flat Front Slim Pant</a>
            <dl class="basket-minicart-tile-properties">
                <dt class="basket-minicart-tile-property-label color-label">Color</dt>
                <dd class="basket-minicart-tile-property-value color-value">Laurel</dd>

                <dt class="basket-minicart-tile-property-label size-label">Size</dt>
                <dd class="basket-minicart-tile-property-value size-value">12</dd>

                <dt class="basket-minicart-tile-property-label price-label">Price</dt>
                <dd class="basket-minicart-tile-property-value price-value">73.99 USD</dd>

                <dt class="basket-minicart-tile-property-label quantity-label">Quantity</dt>
                <dd class="basket-minicart-tile-property-value quantity-value">2</dd>

                <dd class="basket-minicart-tile-price total">147.98 USD</dd>
            </dl>
        </div>
        <div class="basket-minicart-tile">
            <ins class="basket-minicart-tile-decor"></ins>
            <a class="basket-minicart-tile-img" href="/test/index/productPage/?pid=25502038"><img src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10208949.JJ0NLA0.PZ.jpg" alt="Flat Front Slim Pant"></a>
            <a class="basket-minicart-tile-name" href="/test/index/productPage/?pid=25502038">Flat Front Slim Pant</a>
            <dl class="basket-minicart-tile-properties">
                <dt class="basket-minicart-tile-property-label color-label">Color</dt>
                <dd class="basket-minicart-tile-property-value color-value">Laurel</dd>

                <dt class="basket-minicart-tile-property-label size-label">Size</dt>
                <dd class="basket-minicart-tile-property-value size-value">12</dd>

                <dt class="basket-minicart-tile-property-label price-label">Price</dt>
                <dd class="basket-minicart-tile-property-value price-value">73.99 USD</dd>

                <dt class="basket-minicart-tile-property-label quantity-label">Quantity</dt>
                <dd class="basket-minicart-tile-property-value quantity-value">2</dd>

                <dd class="basket-minicart-tile-price total">147.98 USD</dd>
            </dl>
        </div>
    </div>
    <div class="basket-minicart-total">
        <span class="basket-cart-totalitems"><?php echo lang::get()->getVal('items in cart', array('count'=>8));?></span><span class="basket-cart-totalprice"><?php echo lang::get()->getVal('Total in the cart:');?> <b class="totalprice-value">1,387.00 USD</b></span>
    </div>
    <div class="basket-minicart-actions">
        <a class="basket-minicart-tocart" href="#"><?php echo lang::get()->getVal('My Cart');?></a>
        <a class="basket-minicart-tocheckout" href="#"><?php echo lang::get()->getVal('Checkout');?></a>
    </div>
    <div class="basket-minicart-banner"><img src="/modules/default/static/img/banner-003-freeshipping-on-minicart.png" alt="Free shipping on all orders over $99"></div>
</div>
<?php /*
<a href="<?php echo $url->getUrl("basket")?>">
	<div class="basket_top">
	<?php echo $lang->getVal("Your Shopping Cart")?>: <span id="top_basket_quantity"><?php echo $this->quantity; ?></span>
		<span <?php if($this->quantity == 0) echo 'style="display: none"' ?> id="top_basket_info"><?php echo $lang->getVal("items")?>, <?php echo $lang->getVal("total")?>: 
			<span id="top_basket_total_price">
				<?php echo $this->total_price; ?>
			</span> 
			<span id="top_basket_currency">
				<?php echo $this->currency ?>
			</span>
		</span>
	</div>
</a>
 */