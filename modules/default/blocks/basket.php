<?php
class basketBlock extends blockAbstract	{
	public function get() {		
		if(!is_null($_SESSION["basket"])) {
			$basket = json_decode($_SESSION["basket"], 1);
			$this->view->currency = $basket["currency"];
			$this->view->total_price = $basket["product_total"];
			$quantity = 0;
			foreach($basket["product_items"] as $item) {
				$quantity += $item["quantity"];
			}
			$this->view->quantity = $quantity;
			//echo "<pre>".print_r($basket, 1)."</pre>";			
		}
		else {
			$this->view->quantity = 0;
			$this->view->total_price = 0;
			$this->view->currency = 0;
		}
	}
}
?>