var searchScript = function(searchObject) {
	//console.log(searchObject);
	$(document).ready(function(){
		$('#search_result').delegate('span[data-ajax=1]', 'click', function(){
			searchReload($(this).attr("data-page"));
			return false;
		});
		$('#search_result').delegate('.sorting-option', 'click', function(){
			searchObject["sort"] = $(this).attr("data-id");
			searchReload();
		});
		$('#search_result').delegate('[name=items-on-page-select]', 'change', function(){
			searchReload(1, $(this).val());
		});
		$("#enable-refinements").delegate('[name=pref-val]', 'click', function(){
			var selRefAttr = $(this).parent().parent().attr('data-id');
			if(selRefAttr == "c_refinementColor") {
				if(searchObject["selRefs"][selRefAttr]) {					
					searchObject["selRefs"][selRefAttr] = searchObject["selRefs"][selRefAttr]+"|"+$(this).attr('data-id');
				} 
				else
					searchObject["selRefs"][selRefAttr] =  $(this).attr('data-id');
			}
			else
				searchObject["selRefs"][selRefAttr] = $(this).attr('data-id');
			searchReload();
		});
	});
	$('.selected-refinements').delegate('[name=sel-ref-val]', 'click', function(){
		var refAttr = $(this).parent().parent().parent().attr('data-id'),
			attrValue = $(this).attr("data-id");
		if(refAttr == "cgid"){
			if(!searchObject["selRefs"][refAttr].match(/\-\w+$/))
				return false;
			searchObject["selRefs"][refAttr] = searchObject["selRefs"][refAttr].replace(/\-\w+$/, '');
		} 
		else if(refAttr == "c_refinementColor") {
			if(searchObject["selRefs"][refAttr].match(/\|/)) {				
				var reg = new RegExp('(?:\\|'+attrValue+')|(?:'+attrValue+'\\|)');
				searchObject["selRefs"][refAttr] = searchObject["selRefs"][refAttr].replace(reg, '');
			}
			else {
				delete (searchObject["selRefs"][refAttr]);
			}
		}
		else
			delete (searchObject["selRefs"][refAttr]);
		searchReload();
	});
	var searchReload = function(page, itemsOnPage){
		var params = searchObject["selRefs"];	
		params["page"] = page || 1;
		params["count"] = itemsOnPage || searchObject["paging"]["count"];
		params["sort"] = searchObject["sort"];
		app.ajax.load({
			url: config.getUrl("indexSearch_formatAjax"),
			type: 'POST',
			data: params,
			success: function(data){
				$('#content').html(data);
			}
		},
		{
			"isOverlay": 1
		});
	}
	var updateRefinements = function(newRefs) {
		searchObject = newRefs;		
	}
	return {
		searchObject : searchObject,
		updateRefinements : updateRefinements
	}
}