var app = function(){
	var showSuccessMessage = function(message){
		$('#ajax-success-message').html(message);
		$('#ajax-success-message').show();
		setTimeout(function(){
			$('#ajax-success-message').fadeOut(500, function(){
				$('#ajax-success-message').html("");
			});
		}, 4000);
	};
	var showErrorMessage = function(message) {
		$('#ajax-error > .message').html(message);
		$('#loader_image').hide();
		$('#ajax-error').show();
	}
	
	$(document).ready(function(){
		$('#ajax-error-continue').bind('click', function(){
			$('#overlay').hide();
			$('#ajax-error > .message').html("");
			$('#loader_image').show();
			$('#ajax-error').hide();
		})
	});
	return {		
		ajax : {
			load : function(params, myParams) {
				if(myParams.isOverlay){
					$('#overlay').show();
					var complete_function = params.complete;
					params.complete = function(data){
						if(complete_function)
							complete_function(data);		
						$('#overlay').hide();
					}					
				}				
				var success_function = params.success;
				params.success = function(data){
					if(myParams.errorMessage){
						var res = $.parseJSON(data)
						if(res.error == 1){ 
							showErrorMessage(res.message);	
							return false;
						}
					}
					if(success_function)
						success_function(data);
					if(myParams.SuccessMessage)
					showSuccessMessage(myParams.SuccessMessage);										
				}
				if(myParams.errorMessage) {
					var error_function = params.error;
					params.error = function(data){
						if(error_function)
							error_function(data);
						showErrorMessage(myParams.Error);
					}
				}
				$.ajax(params);
			}
		}
	}
}()