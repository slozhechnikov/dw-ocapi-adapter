var basketScript = function(basket) {
	var updateTopBasket = function() {
		$('#top_basket_total_price').html(basket.product_total);
		var total_quantity = 0;
		$.each(basket.product_items, function(k, v){
			total_quantity += v.quantity;
		}); 
		$('#top_basket_quantity').html(total_quantity);
	}
	var itemRollbackQuentity = function(element) {
		element.val(basket.product_items[element.attr('val')].quantity);
	}
	$(document).ready(function(){
		$('[name=remove_item]').bind('click', function(){
			var params = {itemKey : $(this).attr('val')};
			var this_element = this;
			var this_intVal = parseInt($(this).attr('val'));
			app.ajax.load({
				url : config.getUrl("basketRemoveItem"),
				type : "GET",
				data : params,
				success : function(data){
					var res = $.parseJSON(data);
					if(!res.currency) {
						return false;
					}
					basket = res;
					updateTopBasket();				
					$(this_element).parent().parent().remove();
				/*	if(total_quantity == 0) {
						$('#top_basket_info').hide();
						$('.basket_container > table').hide();
					}*/
					$('[val]').each(function(){
						var item_val = parseInt($(this).attr('val'));
						if(item_val > this_intVal) {
							$(this).attr('val', --item_val);
						}
					});
				}
			},
			{
				"isOverlay": 1,
				"SuccessMessage" : config.getLang("product_removed_from_cart"),
				"errorMessage": "Error"
			}
			);
		});
		
		$('[name=Qty]').bind('change', function(){
			var thisElement = $(this);
			if(!$(this).val().match(/^\d+$/)) {
				itemRollbackQuentity(thisElement);
				return false
			}
			var params = {itemKey : $(this).attr('val'), qty : $(this).val()};
			var thisVal = $(this).attr('val');
			app.ajax.load({
				url : config.getUrl("basketUpdateQty"),
				type : "GET",
				data : params,
				success : function(data){
					var res = $.parseJSON(data);
					if(!res.currency) {
						itemRollbackQuentity(thisElement);
						return false;
					}
					basket = res;
					updateTopBasket();
					$('[name=item_price][val='+thisVal+']').html(basket.product_items[thisVal].price);
				}
			},
			{
				"SuccessMessage" : config.getLang("product_quantity_changed"),
				"errorMessage": "Error"
			});
		});
	});
};