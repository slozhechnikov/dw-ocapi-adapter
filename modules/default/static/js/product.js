var prosuctScript = function (productObject) {
	console.log(productObject);
	$(document).ready(function () {
		$('.product_options').bind('change', function () {
			var price_span = $('.full_price > span');
			price_span.html((parseFloat(price_span.attr('product_price')) + parseFloat($(this).val().match(/%(.+)$/)[1])).toFixed(2));
		});

		$('[name=add_to_basket_button]').bind('click', function () {
			var params = {
				"product_id":$('input[name=PID]').val(),
				"quantity"  :parseFloat($('input[name=quantity]').val()).toFixed(2)
			};
			$('.product_options').each(function (k, v) {
				params["options[" + k + "]"] = $(this).val().match(/^[^%]+/)[0];
			});
			app.ajax.load({
					url    :config.getUrl("basketAdd"),
					type   :'POST',
					data   :params,
					success:function (data) {
						var basket = $.parseJSON(data);
						if (basket.product_total) {
							$('#top_basket_total_price').html(basket.product_total.toFixed(2));
							$('#top_basket_currency').html(basket.currency);
							var total_quantity = 0;
							$.each(basket.product_items, function (k, v) {
								total_quantity += v.quantity;
							});
							$('#top_basket_quantity').html(total_quantity);
							$('#top_basket_info').show();
						}
					}
				},
				{
					"isOverlay"     :1,
					"SuccessMessage":"Product added to cart",
					"errorMessage"  :"Error"
				});
				return false;
		});

		$('[name=product_img_preview]').bind('click', function (e) {		
			$('#product_large_img').attr('src', $(this).attr('src').replace(/small/, 'large')).attr('num', $(this).attr('num'));
			//alert($(this).attr('src').replace(/small/, 'large'));
			$('[name=product_img_preview]').removeClass('selected');
			$(this).addClass('selected');
		});

		$('.variant-item').bind('click', function () {
			if (!$(this).attr('data-id')) {
				$('[name=add_to_basket_button]').prop("disabled", true);
				return true;
			}
			$('.variant-item[name='+$(this).attr('name')+']').removeClass('selected');
			$(this).addClass("selected");
			var currentVariant = selectVariant();
			if (currentVariant) {
				$('[name=PID]').val(currentVariant.product_id);
				$('[name=product_price]').val(currentVariant.price);
				//console.log($('.full_price > span');'));
				$('.full_price > span').text(currentVariant.price);
				$('[name=add_to_basket_button]').prop("disabled", false);
			}
			else {
				$('[name=add_to_basket_button]').prop("disabled", true);
			}
			if ($(this).attr('name') == 'color') {
				var currentColor = $(this).attr('data-id');
				$.each(productObject.image_groups, function (k, v) {
					if (v.variation_value == currentColor) {
						if (v.view_type == "large") {
							$('#product_large_img').attr("src", v.images[$('#product_large_img').attr('num')].link);
						}
						else if (v.view_type == "small") {
							$.each(v.images, function (k, v) {
								$('[name=product_img_preview][num=' + k + ']').attr('src', v.link);
							});
						}
					}
				});
			}
		});
	});

	function selectVariant() {
		var opt;
		$.each(productObject.variants, function (k, v) {
			var ok = 1;
			$.each(v.variation_values, function (key, val) {
				if ($('.variant-item.selected[name=' + key + ']').attr('data-id') != val) {
					ok = 0;
					return false;
				}
			});
			if (ok == 1) {
				opt = v;
				return false;
			}
		});
		return opt;
	}
}