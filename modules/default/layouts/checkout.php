<html>
	<head>
	 <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <title>Astound</title>
    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/base.css")?>" media="Screen" type="text/css" />
	<link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/reset.css")?>" media="Screen" type="text/css" />
    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/mobile.css")?>" media="handheld, only screen and (max-width: 480px), only screen and (max-device-width: 480px)" type="text/css" />
	<?php
	$this->view->partial("app/view/scripts/config_js.php");
	?>
	<?php 
	
	$this->includeJS();
	$this->view->includeJS();
	
	?>
	</head>
	<body>
		<div class="checkout-head-steps">
			<?php echo $this->blocks["checkoutSteps"]->show()?>
		</div>
		<div class="content">
		<?php  $this->view->show();?>
		</div>
	</body>
</html>