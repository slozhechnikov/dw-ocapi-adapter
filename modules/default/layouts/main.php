<?php
/**
 * @var layout $this
 * @var view   $this->view
 *
 */
?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes"/>
    <title><?php echo lang::get()->getVal('Logotype alt');?></title>
    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/reset.css")?>"
          media="Screen" type="text/css">

    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/mobile.css")?>"
          media="handheld, only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
          type="text/css">
    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/css/default.css")?>" media="Screen"
          type="text/css">
    <?php
    $this->view->partial("app/view/scripts/config_js.php");
    ?>
    <?php

    $this->includeJS();
    $this->view->includeJS();
	$url = urlService::get();

    ?>
    <!--[if IEMobile]>
    <link rel="stylesheet" href="/<?php echo filePathService::get()->getPath("static/stylesheets/mobile.css")?>" media="screen" type="text/css">
    <![endif]-->

    <!-- These are Open Graph tags.  They add meta data to your  -->
    <!-- site that facebook uses when your content is shared     -->
    <!-- over facebook.  You should fill these tags in with      -->
    <!-- your data.  To learn more about Open Graph, visit       -->
    <!-- 'https://developers.facebook.com/docs/opengraph/'       -->
</head>
<body>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=331133333643837";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="global-container">
    <div class="global-header">
        <div class="global-wrapper">
            <a class="head-logo" href="/" title="<?php echo lang::get()->getVal('Logotype title');?> - Home"><?php echo lang::get()->getVal('Logotype alt');?></a>

            <div class="personal-links"> <!--<a href="#">My Account</a> <a href="#">Order Tracking</a> <a href="#">Wish List</a>--> </div>

            <div class="searchbox">
                <?php if (!$_POST["q"])
                        $this->blocks["search"]->show();?>
            </div>

            <div class="personal-login"> <!--Welcome <a href="#">[Sign In]</a> or <a href="#">[Create an Account]</a>--> </div>

            <div class="shipping-promo"></div>

            <div class="personal-basket">
                <?php $this->blocks["basket"]->show();?>
            </div>
        </div>
    </div>
    <div class="global-head-menu">
        <div class="global-wrapper">
            <?php $this->blocks["head"]->show(); ?>
        </div>
    </div>
    <div class="global-breadcrumb">
        <div class="global-wrapper">
        <?php if($this->view->breadCrumbs) {
        //print_r($this->view->breadCrumbs)
        ?>
            <ol class="breadcrumbs-box">
                <li class="breadcrumbs-item"><a href="/">Home</a></li>
                <?php
				foreach($this->view->breadCrumbs as $breadCrumb) {
				?>
				<li class="breadcrumbs-item"><a href="<?php echo $url->getUrl("index", "search", array("cgid"=>$breadCrumb["id"]))?>">
					<?php echo $breadCrumb["name"] ?>
				</a></li>
				<?php } ?>
            </ol>
            <?php }?>
        </div>
    </div>
    <div class="global-content">
        <div class="global-wrapper" id="content">
            <?php $this->view->show(); ?>
        </div>
    </div>
    <div class="global-footer">
        <div class="global-wrapper">
            <dl class="footer-links-list">
                <dt class="footer-links-headline"><b><a href="/support/">Support</a></b></dt>
                <dd class="footer-links-item">customerser@ocapi.astoundcommerce.com</dd>
                <dd class="footer-links-item">1-888-888-OCAPI</dd>
                <dd class="footer-links-item">Live Chat</dd>
                <dd class="footer-links-item last">Track Your Order</dd>
            </dl>
            <dl class="footer-links-list">
                <dt class="footer-links-headline"><b>Shipping &amp; Returns</b></dt>
                <dd class="footer-links-item">Shipping Policy</dd>
                <dd class="footer-links-item">Cancellations &amp; Changes</dd>
                <dd class="footer-links-item">Returns</dd>
                <dd class="footer-links-item last">My Account</dd>
            </dl>
            <dl class="footer-links-list">
                <dt class="footer-links-headline"><b>Resources</b></dt>
                <dd class="footer-links-item">Terms</dd>
                <dd class="footer-links-item">Privacy &amp; Security</dd>
                <dd class="footer-links-item">FAQ</dd>
                <dd class="footer-links-item last">Site Maps</dd>
            </dl>
        </div>
    </div>

    <div id="overlay" class="global-overlay">
        <div id="loader_image" class="global-ajax-loader"></div>
    </div>
    <div id="ajax-error" class="global-ajax-error">
        <div class="message"></div>
        <div id="ajax-error-continue" class="pointer m-ajax-error-continue">
            <?php echo lang::get()->getVal("ajax_error_continue")?>
        </div>
    </div>
    <div id="ajax-success-message"></div>
</div>

</body>
</html>
