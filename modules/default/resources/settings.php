<?php

class settings {
	
	private static $values = array(
		"DEFAULT_ITEMS_ON_PAGE"=>12,	
		"language"=>"en",
		"ITEMS_ON_PAGE_OPTIONS"=>array(12, 24, 48)
	);	
	
	public static function get($key) {
		return self::$values[$key];
	}
}
?>