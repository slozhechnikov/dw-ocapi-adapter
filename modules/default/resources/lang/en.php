<?php
$LANG = array(
    "cart" => "basket",
    "items" => "items",
    "total" => "Total",
    "searchlabel" => "Search \\",
    "searchdefaultvalue" => "Enter term or Item #",
    "searchsubmit" => "\\ Go",
    "product" => "Product",
    "quantity" => "Quantity",
    "quantity_short" => "Qty",
    "price" => "Price",
    "options" => "Options",
    "remove" => "Remove",
    "pdp_add_to_cart" => "Add to basket",
    "select" => "select",
    "sorting" => "Sorting",
    "product_added_to_cart" => "Product added to cart",
    "no_products" => "No products",
    "product_removed_from_cart" => "Product removed from cart",
    "product_quantity_changed" => "Product quantity changed",
    "ajax_error_continue" => "continue",
    "update_basket_error" => "An error occurred while updating basket",
    "checkout" => "checkout",
    "Your Shopping Cart" => "Your Shopping Cart",
    "Expand cart" => "Expand cart",
    "Search By: Category..." => "Search By: Category, Brand, Color, Price, Popular Items",

    /*** Main lyout ***/
    'Logotype alt' => 'F Commerce OCAPI',
    'Logotype title' => 'F Commerce OCAPI',

    /*** Minicart ***/
    'items in cart' => '%count% items in cart',
    'Total in the cart:' => 'Total:',

    /*** Pagination texts ***/
    'Displaying products from-to of total' => 'Displaying products %from-to% of %total%',
    'First page' => 'First page',
    'Previous page' => 'Previous page',
    'Next page' => 'Next page',
    'Last page' => 'Last page',
    'per page' => '%count% per page',

    /*** Product list page ***/
    'Reviews' => '%count% Reviews',
    'Price Now' => 'Now:',
    'Hot Deal' => 'Hot Deal',

    /*** Product detail page***/
    'Total customers reviews' => '(%count% customers reviews)',
    'Our Price' => 'Our Price',
    'List price' => 'List price',
    'You save %' => '(you save %count%%)',
    'PDP: Add to cart' => 'Add to cart',

);
?>