<?php
class lang {
	
	private $langArray;
	
	private static $instance;
	
	private function __construct() {
		require_once(filePathService::get()->getPath("resources/lang/".settings::get("language").".php"));
		$this->langArray = $LANG;
	}
	
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new lang();
		}
		return self::$instance;
	}
	
	public function getVal($key, $params=null) {
		if(!$this->langArray[$key]) return $key;
		if(!$params) return $this->langArray[$key];
		if(is_array($params)) {
			foreach($params as $k=>$v) {
				$paramsArray["%".$k."%"] = $v;
			}
			return strtr($this->langArray[$key], $paramsArray);
		}
		else 
			return preg_replace('/%val%/', $params, $this->langArray[$key]);
	}
}
?>