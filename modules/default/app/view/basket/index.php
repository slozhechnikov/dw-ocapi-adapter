<?php 
$url = urlService::get();
$lang = lang::get();
?>
<div class="global-left-column"></div>
<div class="global-right-column">
    <div class="global-right-wrapper">
        <h1 class="global-page-title"><?php echo lang::get()->getVal('Your Cart');?></h1>
        <?php $this->partial("app/view/basket/partials/basketTable.php", array("basket"=>$this->basket)); ?>
        <div class="checkout-button">
            <a href="<?php echo urlService::get()->getUrl("checkout", "index")?>"> <?php echo lang::get()->getVal('checkout');?> </a>
            <?php /*
	<a href="javascript:window.open('<?php echo facebookService::get()->getLoginUrl(array("redirect_uri"=>urlService::get()->getFullUrl("checkout", 'fbAuth'), "scope"=>array("email","user_birthday","user_location")))?>','JSSite','width=420,height=230,resizable=yes,scrollbars=yes,status=yes')">
	<?php echo $lang->getVal("checkout")?>
	</a>
	*/?>
        </div>
    </div>
</div>

<?php  $this->partial("app/view/basket/scripts/basketjson.php", array('basketJSON'=>$this->basketJSON)) ?>