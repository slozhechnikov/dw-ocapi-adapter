<?php
$url = urlService::get();
$lang = lang::get();
?>
<div class="cart-box">
    <table border="0" class="cart-table" >
        <thead>
        <tr>
            <th class="cart-head cart-head-name"><?php echo $lang->getVal("product")?> </th>
            <th class="cart-head cart-head-quantity"><?php echo $lang->getVal("quantity_short")?> </th>
            <th class="cart-head cart-head-price"><?php echo $lang->getVal("price")?> </th>
            <?php if (!$this->basket['billing_address']) { ?>
                <th class="cart-head cart-head-options"><?php echo $lang->getVal("options")?> </th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($this->basket["product_items"] as $key => $item) {
            $optionsSumPrice = 0;
            if ($item["option_items"]) {
                foreach ($item["option_items"] as $option_item) {
                    $optionsSumPrice += $option_item["price"];
                }
            }
            ?>
            <tr>
                <td class="cart-body-cell cart-body-name">
                    <a href="<?php echo $url->getUrl("index", "productPage", array("pid" => $item["product_id"]))?>"><?php echo $item["product_name"] ?></a>
                </td>
                <td class="cart-body-cell cart-body-quantity">
                    <?php if (!$this->basket['billing_address']) { ?>
                    <input name="Qty" val="<?php echo $key?>" style="width:20px"
                           value="<?php echo $item["quantity"] ?>">
                    <?php
                } else {
                    ?>
                    <span>
                    <?php echo $item["quantity"] ?>
                    </span>
                    <?php
                }
                    ?>
                </td>
                <td class="cart-body-cell cart-body-price">
                    <span name="item_price" val="<?php echo $key?>"><?php echo $item["price"] + $optionsSumPrice; ?></span>
                </td>
                <?php if (!$this->basket['billing_address']) { ?>
                <td class="cart-body-cell cart-body-options">
                    <div name="remove_item" val="<?php echo $key?>"
                         class="removeItem cart-remove-btn"><?php echo $lang->getVal("remove")?></div>
                </td>
                <?php } ?>
                <?php /* <div class="item_block">
				<div class="item_name">
					<?php echo $item["product_name"] ?>
				</div>
				<div>
					<img src=<?php echo $this->products["products"]["data"][$key]["image_groups"][0]["images"][0]["link"] ?>>
				</div>
				<div>
					price: <span><?php echo $item["price"]?></span>
				</div>
			</div> */?>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
