<?php 
$url = urlService::get();
$lang = lang::get();
?>
<script>
var config = function() {
	var urls = {
		"basketRemoveItem" 		 : "<?php echo $url->getUrl("basket", "removeItem")?>",
		"basketUpdateQty"  		 : "<?php echo $url->getUrl("basket", "updateQty")?>",
		"basketAdd"              : "<?php echo $url->getUrl("basket", "add")?>",
		"indexSearch_formatAjax" : "<?php echo $url->getUrl("index", "search", array("format"=>"ajax"))?>"
	}
	
	var lang = {
		"product_added_to_cart"     : "<?php echo $lang->getVal("product_added_to_cart")?>",
		"product_removed_from_cart" : "<?php echo $lang->getVal("product_removed_from_cart")?>",
		"product_quantity_changed"  : "<?php echo $lang->getVal("product_quantity_changed")?>"
	}
	
	return {
		getUrl : function(url) {
			return urls[url];
		},
		getLang : function(key) {
			return lang[key] || key;
		}
	}
}();
</script>