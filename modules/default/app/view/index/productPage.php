<?php
/**
 * @ var indexController $this
 */
$url = urlService::get();
$lang = lang::get();
$images = array();
foreach ($this->product["image_groups"] as $image_group) {
    if ($image_group["view_type"] != "small")
        continue;
    if ($image_group["variation_value"] != "")
        continue;
    foreach ($image_group["images"] as $image)
        $images[] = $image;
}

foreach ($images as &$image) {
    /* Removing hardcoded protocol can be better solution if Demandware can provide file in both http & https protocols.
     * Browser can make choice by self which one is using guided by whole frame protocol.
     * So result will be: https://www.. cropped to //www.. & http://www.. cropped to //www.. also.
     */
    $image["small"] = preg_replace('/http(s?):/', '', $image["link"]);
    $image["large"] = preg_replace('/small/', 'large', $image["small"]);
}

?>
<div class="product-detail-right-column">
    <div class="product-detail-freeshipping-banner"><img src="/modules/default/static/img/banner-002-freeshipping-on-PDP.png"
                                            alt="Free Shipping On Orders over $99"></div>
   <?php $this->partial("app/view/index/partials/youMayAlsoLike.php", array("YMALProducts"=>$this->YMALProducts["hits"])) ?>
    <div class="product-detail-shortlist recently-viewed">
        <h3 class="product-detail-shortlist-title"><?php echo lang::get()->getVal('Recently viewed');?></h3>

        <div class="product-detail-shortlist-box">
            <div class="product-detail-shortlist-tile">
                <a class="product-detail-shortlist-image" href="/test/index/productPage/?pid=25565789"><img
                        src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10230296.JJI15XX.PZ.jpg"
                        alt="Long Tired Skirt"></a>
                <a class="product-detail-shortlist-name" href="/test/index/productPage/?pid=25565789">Long Tired
                    Skirt</a>

                <div class="product-detail-shortlist-rating">
                    <div class="rating rating-1"></div>
                </div>
                <div class="product-detail-shortlist-price">
                    <s class="product-detail-shortlist-priceold priceold">72,000 USD</s>
                    <i class="product-detail-shortlist-pricenew pricenew">Now <strong>92,000 USD</strong></i>
                </div>
            </div>
            <div class="product-detail-shortlist-tile">
                <a class="product-detail-shortlist-image" href="/test/index/productPage/?pid=25565789"><img
                        src="//demo.ocapi.demandware.net/on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1345174669086/images/large/PG.10230296.JJI15XX.PZ.jpg"
                        alt="Long Tired Skirt"></a>
                <a class="product-detail-shortlist-name" href="/test/index/productPage/?pid=25565789">Long Tired
                    Skirt</a>

                <div class="product-detail-shortlist-rating">
                    <div class="rating rating-1"></div>
                </div>
                <div class="product-detail-shortlist-price">
                    <s class="product-detail-shortlist-priceold priceold">72,000 USD</s>
                    <i class="product-detail-shortlist-pricenew pricenew">Now <strong>92,000 USD</strong></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-detail-left-column">
    <div class="product-detail-left-right-column">
        <div class="product-detail-title-n-price-box">
            <h1 class="product-detail-product-title"><?php echo $this->product["name"] ?></h1>

            <div class="product-detail-product-brand"></div>
            <div class="product-detail-product-rating">
                <span class="rating rating-3-5"></span>
                <span class="desc"><?php echo lang::get()->getVal('Total customers reviews', array('count' => 15));?></span>
            </div>
            <div class="product-detail-product-price">
                <em class="product-detail-product-pricenew pricenew"><?php echo lang::get()->getVal('Our Price');?>
                    <strong class="full_price">
                        <span product_price="<?php echo $this->product["price"]?>"><?php echo $this->product["price"]?></span>
                        <?php echo $this->product["currency"] ?>
                    </strong></em>
                <s class="product-detail-product-priceold priceold"><?php echo lang::get()->getVal('List price');?> <?php echo $this->product["price"]?> <?php echo $this->product["currency"] ?></s>
                <i class="product-detail-product-pricediscount pricediscount"><?php echo lang::get()->getVal('You save %', array('count' => 20));?></i>
            </div>
            <div class="product-detail-product-availability">
                <i class="availability-instock"><?php echo lang::get()->getVal('In Stock');?></i> <span
                    class="desc"><?php echo lang::get()->getVal('(Ships in 1-5 days)');?></span>
            </div>
            <?php /*
            <a class="product-detail-product-button"
               href="#"><?php echo lang::get()->getVal('Add personalization');?></a>
            */ ?>
        </div>
        <div class="product-detail-addtocart">
            <?php //echo "<pre>".print_r($this->product["variation_attributes"], 1)."</pre>"?>
            <form class="product-detail-addtocart-form" action="">
			<?php if (!empty($this->product["variation_attributes"])) { ?>
			<?php if($this->product["variation_attributes"]["color"]) {?>
                <fieldset class="product-detail-variant-box variant-list-color">
                    <h4 class="product-detail-variant-title"><?php $this->product["variation_attributes"]["color"]["name"]?></h4>
                    <ul class="product-detail-variant-list">
			<?php foreach($this->product["variation_attributes"]["color"]["values"] as $colorVariant){ ?>
                        <li name="color" class="variant-item variant-<?php echo $colorVariant["name"] ?>" data-id="<?php echo $colorVariant["value"] ?>" title="<?php echo $colorVariant["name"] ?>"><span><?php echo $colorVariant["name"] ?></span></li>
			<?php } ?>                   
                    </ul>
                </fieldset>
			<?php } ?>
			<?php 
				foreach($this->product["variation_attributes"] as $key=>$value) {
					if($key == "color") continue;
			?> 
                <fieldset class="product-detail-variant-box variant-list-size">
                    <h4 class="product-detail-variant-title">Size (<a href="#accessories">size chart</a>)</h4>
                    <ul class="product-detail-variant-list">
					<?php foreach($value["values"] as $sizeVariant){ ?>
                        <li name="<?php echo $key ?>" class="variant-item variant-<?php echo $sizeVariant["name"] ?>"  data-id="<?php echo $sizeVariant["value"] ?>" title="<?php echo $sizeVariant["name"] ?>">
						<span><?php echo $sizeVariant["name"] ?></span></li>
					<?php } ?>              
                    </ul>
                </fieldset>
			<?php } ?>
               <?php } ?>
                   
                <?php if (!empty($this->product["options"])) { ?>
                <?php foreach ($this->product["options"] as $option) { ?>
                    <fieldset class="product-detail-options-box option-<?php echo $option["id"]?>">
                        <label for="option-<?php echo $option["id"]?>"><?php echo $option["name"]?></label>
                        <select class="product_options" name="<?php echo $option["id"]?>"
                                id="option-<?php echo $option["id"]?>">
                            <option value="">...</option>
                            <?php foreach ($option["values"] as $opt_value) { ?>
                            <option <?php if ($opt_value["default"] == 1) echo "selected" ?>
                                    value="<?php echo $opt_value["id"] . "%" . $opt_value["price"]?>">
                                <?php echo $opt_value["name"]?>
                            </option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    <?php } ?>
                <?php } ?>
                <fieldset class="product-detail-quantity">
                    <label class="product-detail-quntity-title" for="quantity"><?php echo $lang->getVal("quantity")?></label>
                    <input type="number" name="quantity" id="quantity" value="1" min="1" max="999" maxlength="3">
                </fieldset>
                <input type="hidden" name="PID" value="<?php echo $this->product["id"]?>">
                <?php if ($this->product["master"]) { ?>
                <input type="hidden" name="master_id" value="<?php echo $this->product["master"]["master_id"] ?>">
                <?php } ?>
                <button name="add_to_basket_button"
                        class="product-detail-add-to-basket"
                        type="submit"
                    <?php if ($this->product["type"]["master"] == 1) echo "disabled";?>
                        ><?php echo $lang->getVal("PDP: Add to cart")?></button>

            </form>
        </div>
        <div class="product-detail-description-box">
            <h3 class="product-detail-description-title"><?php echo lang::get()->getVal('Product description');?></h3>

            <div class="product-detail-description">
                <?php echo $this->product["long_description"] ? $this->product["long_description"] : $this->product["short_description"] ?>
            </div>
            <div class="product-detail-additional-tabs tabify">
                <ul class="product-detail-tabs-menu tabify-menu">
                    <li class="selected"><a href="#specification"><?php echo lang::get()->getVal('Specification');?></a>
                    </li>
                    <li><a href="#warranty"><?php echo lang::get()->getVal('Warranty info');?></a></li>
                    <li><a href="#accessories"><?php echo lang::get()->getVal('Accessories');?></a></li>
                </ul>
                <div id="specification" class="product-detail-tab-content selected">Specification</div>
                <div id="warranty" class="product-detail-tab-content">Warranty info</div>
                <div id="accessories" class="product-detail-tab-content">Accessories</div>
            </div>
        </div>
    </div>
    <div class="product-detail-left-left-column">
        <div class="product-detail-images-box">
            <div class="product-detail-images-huge"><img
                    id="product_large_img"
                    num="0"
                    src="<?php echo $images[0]["large"] ?>"
                    alt="<?php echo $images[0]["alt"] ?>"></div>
            <div class="product-detail-images-previewlist">

                <?php foreach ($images as $number => $image1) { ?>
                <?php //print_r($image1["small"])?>
                <img name="product_img_preview"
                     title="<?php echo $image1["title"]?>"
                     class="product-detail-image-preview product_img_preview <?php echo $number == 0 ? 'selected' : '';?>"
                     num="<?php echo $number; ?>" src="<?php echo $image1["small"] ?>"
                     alt="<?php echo  $image1["alt"] ?>">
                <?php } ?>
            </div>
        </div>
<?php /*
        <div class="product-detail-promotional">
            <h3 class="product-detail-promotional-title"><?php echo lang::get()->getVal('Optional promotional messaging');?></h3>
            <a class="product-detail-product-button" href="#"><?php echo lang::get()->getVal('Add to compare');?></a>
            <a class="product-detail-product-button" href="#"><?php echo lang::get()->getVal('Add to Wishlist');?></a>
            <a class="product-detail-product-button" href="#"><?php echo lang::get()->getVal('Email to a friend');?></a>
            <a class="product-detail-product-button" href="#"><?php echo lang::get()->getVal('Print this page');?></a>
            <a class="product-detail-product-button"
               href="#"><?php echo lang::get()->getVal('Notification options');?></a>
        </div>
*/ ?>
    </div>
    <div class="product-detail-comments">
        <h3 class="product-detail-comments-title"><?php echo lang::get()->getVal('Customer Ratings &amp; Reviews');?></h3>

        <div class="product-detail-comments-box">
            <!-- Product comment & reviews here -->
        </div>
    </div>
</div>
<?php $this->partial("app/view/index/scripts/productjson.php", array('productJSON' => preg_replace('/https/', 'http', $this->productJSON))) ?>
<?php //$this->addToJs('jquery.tabify.source')?>
