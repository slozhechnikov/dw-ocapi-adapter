<?php
/**
 * @template
<div class="p-pagination-box">
    <div class="p-pagination-description">Displaying products 1-12 of 120</div>
    <div class="p-pagination-navigator">
        <span class="p-pagination-title">Page:</span>
        <a class="p-pagination-first" href="" title="First page">First page</a>
        <a class="p-pagination-previous" href="" title="Previous page">Previous page</a>
        <a class="p-pagination-page" href="">10</a>
        <a class="p-pagination-page" href="">11</a>
        <b class="p-pagination-current" >12</b>
        <a class="p-pagination-page" href="">13</a>
        <a class="p-pagination-page" href="">14</a>
        <a class="p-pagination-next" href="" title="Next page">Next page</a>
        <a class="p-pagination-previous" href="" title="Last page">Last page</a>
    </div>
    <label class="p-pagination-perpage">
        <select name="" id="">
            <option value="12">12 per page</option>
            <option value="24">24 per page</option>
            <option value="48">A lot of</option>
        </select>
    </label>
</div>
 *
 *
 * @var indexController $this
 */
$url = urlService::get();
if ($this->search == 1) {
    $link = "";
    $ajax = 1;
    $tag = "span";
} else {
    $link = $url->getUrl("index", "productList", array("cid" => $_GET["cid"]));
    $ajax = 0;
    $tag = "a";
}
$item_on_page = $this->requested_count;
$items_on_page_options = settings::get("ITEMS_ON_PAGE_OPTIONS");
$page_count = ceil($this->total / $item_on_page);
$current_page = ($this->start / $item_on_page) + 1;
$page_start = $current_page > 3 ? $current_page - 2 : 1;
$page_finish = ($current_page < $page_count - 2) ? ($current_page + 2) : $page_count;

$item_start = ($current_page - 1)*$item_on_page;
$item_finish = $item_start + $this->count;
$item_start = $item_start < 1 ? 1 : $item_start;


?>
<div class="pagination-box">
    <div class="pagination-description"><?php echo lang::get()->getVal(
        'Displaying products from-to of total',
        array(
            'from-to'=> $item_start .'-'. $item_finish,
            'total' => $this->total
        )
    );?></div>

    <div class="pagination-navigator">
        <span class="pagination-title"><?php echo lang::get()->getVal('Page:');?></span>
<?php if ($current_page != 1) { ?>
    <<?php echo $tag ?>
    title="<?php echo lang::get()->getVal('First page');?>"
    class="pagination-first"
    data-ajax="<?php echo $ajax ?>"
    data-page="1"
    href="<?php echo $link ?>&page=1"><?php echo lang::get()->getVal('First page');?></<?php echo $tag ?>>
    <<?php echo $tag ?>
    title="<?php echo lang::get()->getVal('Previous page');?>"
    class="pagination-previous"
    data-ajax="<?php echo $ajax ?>"
    data-page="<?php echo $current_page-1;?>"
    href="<?php echo $link ?>&page=<?php echo $current_page-1;?>"><?php echo lang::get()->getVal('Previous page');?></<?php echo $tag ?>>
<?php } else { ?>
<?php } ?>
<?php for ($i = $page_start; $i <= $page_finish; $i++) {
    if ($i != $current_page) {
        ?>
    <<?php echo $tag ?>
    class="pagination-page"
    data-ajax="<?php echo $ajax ?>"
    data-page="<?php echo $i ?>"
    href="<?php echo $link ?>&page=<?php echo $i ?>"><?php echo $i?></<?php echo $tag ?>>
    <?php } else { ?>
    <b class="pagination-current" href=""><?php echo $i?></b>
    <?php
    }
}
?>
<?php if ($current_page != $page_count) { ?>
    <<?php echo $tag ?>
        class="pagination-next"
        data-ajax="<?php echo $ajax ?>"
        data-page="<?php echo $current_page+1 ?>"
        title="<?php echo lang::get()->getVal('Next page');?>"
        href="<?php echo $link ?>&page=<?php echo $current_page+1 ?>"><?php echo lang::get()->getVal('Next page');?></<?php echo $tag ?>>
    <<?php echo $tag ?>
        class="pagination-last"
        data-ajax="<?php echo $ajax ?>"
        data-page="<?php echo $page_count ?>"
        title="<?php echo lang::get()->getVal('Last page');?>"
        href="<?php echo $link ?>&page=<?php echo $page_count ?>"><?php echo lang::get()->getVal('Last page');?></<?php echo $tag ?>>
<?php } else { ?>
<?php } ?>
    </div>
    <label class="pagination-perpage">
        <select name="items-on-page-select" id="">
			<?php 
			foreach($items_on_page_options as $items_on_page_option) {
			?>
				<option <?php echo $items_on_page_option == $item_on_page ? "selected" : "" ?> value="<?php echo $items_on_page_option?>">
					<?php echo lang::get()->getVal('per page', array('count'=>$items_on_page_option));?>
				</option>
			<?php 
			}?>
        </select>
    </label>
</div>
