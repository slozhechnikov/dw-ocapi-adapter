<div class="refinements-box">
    <div class="refinements-selected">
        <?php
        $selRefs = $this->selected_refinements;
        $refs = $this->refinements;
	//	echo "<pre>".print_r($selRefs, 1)."</pre>";
        if ($selRefs["cgid"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["cgid"], "ref" => $refs["cgid"]));
        if ($selRefs["c_refinementColor"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["c_refinementColor"], "ref" => $refs["c_refinementColor"]));
        if ($selRefs["price"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["price"], "ref" => $refs["price"]));
		if ($selRefs["brand"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["brand"], "ref" => $refs["brand"]));
		if ($selRefs["c_tvSize"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["c_tvSize"], "ref" => $refs["c_tvSize"]));
		if ($selRefs["c_tvType"])
            $this->partial("app/view/index/partials/oneSelectedRef.php", array("selRef" => $selRefs["c_tvType"], "ref" => $refs["c_tvType"]));

//$refs);
        ?>
    </div>
    <div class="refinements-enable" id="enable-refinements">
        <?php 
		while (list($k, $v) = each($refs)) { 
			if(empty($v["values"]) || count($v["values"]) == 1) continue;
		?>
        <ul class="refinement <?php echo ( $v['attribute_id'] == 'c_refinementColor' ? 'refinement-color':'');?>">
            <li class="refinement-headline"><?php echo $v["label"];?></li>
            <li class="refinement-values" data-id="<?php echo $v["attribute_id"]?>">
                <?php
                while ($refVal = current($v["values"])) {
                    if ($refVal["value"] == $selRefs[$k] && !($refVal["values"])) {
                        next($v["values"]);
                        continue;
                    }
                    ?>
                    <div class="refinement-value" name="ref-value">
                        <div class="refinement-level-0 <?php echo ( $v['attribute_id'] == 'c_refinementColor' ? strtolower($refVal["label"]):'');?>"
                             name="pref-val"
                             data-id="<?php echo $refVal["value"] ?>"><?php echo $refVal["label"]?></div>
                        <?php
                        if ($refVal["values"])
                        while ($refVel2 = current($refVal["values"])) {
                            if ($refVel2["value"] == $selRefs[$k]) {
                                next($refVal["values"]);
                                continue;
                            }
                            ?>
                            <div class="refinement-level-1"
                                 name="pref-val"
                                 data-id="<?php echo $refVel2["value"] ?>"><?php echo $refVel2["label"] ?></div>
                            <?php
                            next($refVal["values"]);
                        }?>
                    </div>
                    <?php
                    next($v["values"]);
                }
                ?>
            </li>
        </ul>
        <?php
    }
        ?>
    </div>
</div>
