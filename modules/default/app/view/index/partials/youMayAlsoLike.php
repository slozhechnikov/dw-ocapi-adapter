<?php 
$url = urlService::get(); 
//echo "<pre>".print_r($this->YMALProducts, 1)."</pre>"?>
<div class="product-detail-shortlist you-may-also-like">
	<h3 class="product-detail-shortlist-title"><?php echo lang::get()->getVal('You may Also Like');?></h3>
	<div class="product-detaile-shortlist-box">
		<?php 
		foreach($this->YMALProducts as $product) {
			$product_url = $url->getUrl("index", "productPage", array("pid" => $product["product_id"]));
		?>
		<div class="product-detail-shortlist-tile">
			<a class="product-detail-shortlist-image" href="<?php echo $product_url ?>"><img
					src="<?php echo preg_replace('/^https/', 'http', $product["image"]["link"])?>"
					alt="<?php echo $product["image"]["alt"]?>"></a>
			<a class="product-detail-shortlist-name" href="<?php echo $product_url ?>">
				<?php echo $product["product_name"]?>
			</a>

			<div class="product-detail-shortlist-rating">
				<div class="rating rating-1"></div>
			</div>
			<div class="product-detail-shortlist-price">
			<s class="product-detail-shortlist-priceold priceold"><?php echo $product["price"]." ".$product["currency"]  ?> </s>
				<i class="product-detail-shortlist-pricenew pricenew">Now <strong><?php echo $product["price"]." ".$product["currency"]  ?> 
			</strong></i>
			</div>
		</div>
		<?php }?>
	</div>
</div>