<ul class="refinement selected-refinement" data-id="<?php echo $this->ref["attribute_id"]; ?>">
    <li class="refinement-headline"><?php echo $this->ref["label"]; ?></li>
    <li class="refinement-values">
        <?php
        $selRefValues = explode("|", $this->selRef);
        while ($selRefVal = current($selRefValues)) {
            $this->ref["values"][$selRefVal]["current"] = "true";
            ?>
            <div class="refinement-value">
                <?php
                if (strrpos($selRefVal, "-") && !$this->ref["values"][$selRefVal]) {
                    $SelPrefArr = explode("-", $selRefVal);
                    ?>
                    <div class="refinement-level-0"><?php echo $this->ref["values"][$SelPrefArr[0]]["label"]?></div>
                    <div name="sel-ref-val"
                         data-id="<?php echo  $this->ref["values"][$SelPrefArr[0]]["values"][$selRefVal]["value"] ?>"
                         class="refinement-level-1">
                        <?php echo  $this->ref["values"][$SelPrefArr[0]]["values"][$selRefVal]["label"] ?>
                    </div>

                    <?php
                } else {
                    ?>
                    <div name="sel-ref-val"
                         data-id="<?php echo $this->ref["values"][$selRefVal]["value"]?>"
                         class="refinement-level-0"><?php echo $this->ref["values"][$selRefVal]["label"] ?></div>
                    <?php }?>
            </div>
            <?php
            next($selRefValues);
        }
        ?>
    </li>
</ul>