<?php 
$url = urlService::get(); 
$products = $this->products;
?>
<?php echo $this->pages ;?>
    <?php //echo "<pre>".print_r($this->products, 1)."</pre>";
    //echo '<!-- <pre style="color:#EF0000">' . print_r($this, true) . '</pre> -->';
    ?>
<div class="search-results-box"><?php
    while ($product = current($products)) {
        $product_url = $url->getUrl("index", "productPage", array("pid" => $product["product_id"]));
        ?>
<div class="search-results-producttile">
            <a class="search-results-producttile-image"
               href="<?php echo $product_url;?>"
               title="<?php echo htmlspecialchars($product["image"]["title"]) ?>">
                <img src="<?php echo preg_replace('/https/', 'http', $product["image"]["link"])?>"
                     alt="<?php echo htmlspecialchars($product["image"]["title"]) ?>">
            </a>
            <ins class="search-results-producttile-decor"><ins class="search-results-producttile-indecor"></ins></ins>
            <div class="search-results-producttile-footer">
                <div class="search-results-producttile-wrapper">
                    <a class="search-results-producttile-category" href="<?php echo $product_url;?>"></a>
                    <a class="search-results-producttile-name"     href="<?php echo $product_url;?>"><?php echo htmlspecialchars($product["product_name"])?></a>
                    <div class="search-results-producttile-rating"><i class="rating rating-4-5"></i><b class="desc"><?php echo lang::get()->getVal('Reviews', array('count' => 20));?></b></div>
                    <div class="search-results-producttile-price">
                        <span class="search-results-producttile-pricediscount pricediscount"><?php echo lang::get()->getVal('Hot Deal');?> <strong>-25%</strong></span>
                        <s class="search-results-producttile-priceold priceold"><?php echo $product["price"] . " " . $product["currency"] ?></s>
                        <em class="search-results-producttile-pricenew pricenew"><?php echo lang::get()->getVal("Price Now")?> <strong><?php echo $product["price"] . " " . $product["currency"] ?></strong></em>
                    </div>
                    <div class="search-results-producttile-availability l-producttile-availability-instock "><?php echo lang::get()->getVal("in stock")?></div>
                </div>
            </div>
    </div><?php
	next($products);
    }
?></div>
<?php //echo $this->pages . "<br>"; ?>