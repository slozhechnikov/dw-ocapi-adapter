<?php
$url = urlService::get();
$lang = lang::get();

$pageItem = end($this->breadCrumbs);
$pageTitle = $pageItem['page_title'];
?>
<?php
//if($this->format != "ajax") {
?>
<div class="global-left-column">
    <?php
    $this->partial("app/view/index/partials/searchRefinements.php", array(
        "refinements"          => $this->searchResult["refinements"],
        "selected_refinements" => $this->searchResult["selected_refinements"]
    )); ?>
</div>
<div class="global-right-column" id="search_result">
    <div class="global-right-wrapper">
        <h1 class="global-page-title"><?php echo $pageTitle; ?></h1>
        <!--
        <div class="categorylanding-banner">
            <a href="#"><img
                    src="/modules/default/static/img/banner-001-3d-glasses.jpg<?php //echo filePathService::get()->getPath("static/images/banner-001-3d-glasses.jpg", null) // error occur ?>"
                    alt=""></a>
        </div>
        -->

        <div class="page-pagination-top">
            <?php echo $this->pages; ?>
            <div class="pagination-sorting-box">
                <dl class="pagination-sorting">
                    <dt class="pagination-sorting-title"><?php echo lang::get()->getVal('Sort by:');?></dt>
                    <?php foreach ($this->searchResult["sorting_options"] as $option) { ?>
                    <dd class="pagination-sorting-item sorting-option<?php echo $option["id"] == $this->searchResult["selected_sorting_option"] ? "_selected" : " pointer"?>"
                        data-id="<?php echo $option["id"];?>">
                        <span class="pagination-sorting-link"><?php echo $option["label"]?></span>
                    </dd>
                    <?php } ?>
                </dl>
            </div>
        </div>
        <?php //echo "<pre>".print_r($this->searchResult, 1)."</pre>";
        if (empty($this->searchResult["hits"])) {
            echo $lang::get()->getVal("no_products");
        } else {
            $this->partial("app/view/index/productList.php", array("products" => $this->searchResult["hits"]));
        }
        ?>
        <div class="page-pagination-bottom"><?php echo $this->pages; ?></div>
    </div>
</div>
<?php
$this->partial("app/view/index/scripts/searchjson.php", array(
    "searchJSON" => array(
        "selRefs" => $this->searchResult["selected_refinements"],
        "refs"    => $this->searchResult["refinements"],
        "paging"  => array(
            "total" => $this->searchResult["total"],
            "count" => $this->searchResult["requested_count"],
            "start" => $this->searchResult["start"]
        ),
        //"sort" => $this->searchResult["products"]["sorting_options"]
        "sort"    => $this->searchResult["selected_sorting_option"]
    )));
?>