<?
//echo "qrqwerwert";
//echo "<pre>".print_r($this->fbUserInfo, 1)."</pre>";
$url = urlService::get();
$lang = lang::get();
?>
<form method="POST" id="checkout_form" action="<?php echo $url->getUrl("checkout", "order")?>">
<div class="shipping-detail">
	<div class="big">
		Shipping Details
	</div>
	<div class="form-container">	
		<div>
			<span>
				* First Name
			</span>
			<input data-mandatory="1" type="text" name="first_name" value="<?php echo $this->fbUserInfo["first_name"]?>">
		</div>
		<div>
			<span>
				* Last Name
			</span>
			<input data-mandatory="1" type="text" name="last_name" value="<?php echo $this->fbUserInfo["last_name"]?>">
		</div>		
		<div>
			<span>
				* Email
			</span>
			<input data-mandatory="1" type="text" name="email" value="<?php echo $this->fbUserInfo["email"]?>">
		</div>
	</div>
</div>
<div class="shipping-adress">
	<div>
		<span>
			House Number		
		</span>
		<input type="text" name="house_number" value="">
	</div>
	<div>
		<span>
			* Street		
		</span>
		<input data-mandatory="1" type="text" name="street" value="">
	</div>
	<div>
		<span>
			* Phone		
		</span>
		<input data-mandatory="1" type="text" name="phone" value="">
	</div>
	<div>
		<span>
			* City		
		</span>
		<input data-mandatory="1" type="text" name="city" value="">
	</div>
	<div>
		<span>
			Additional Address Information	
		</span>
		<input data-mandatory="1" type="text" name="add_adress" value="">
	</div>
	<div>
		<span>
			* Postal Code
		</span>
		<input data-mandatory="1" type="text" name="postal_code" value="">
	</div>
	<div>
		<span>
			 * Date of Birth
		</span>
		<?php echo $this->showCalendar($this->fbUserInfo["birthday"])?>
	</div>
</div>
<div class="shipping-methods-container">
	<?php 
	foreach($this->shippingMethods["data"] as $method) {
	?>
	<div class="method-block">
		<input type="radio" name="shipping_method" value="<?php echo $method["id"]?>">
		<label>
		<?php echo $method["name"]?>
		<span><?php echo $method["price"]?></span>
		</label>
	</div>
	<? } ?>
</div>
<div>
<input type="submit" value="submit">
</div>
</form>
<?//="<pre>".print_r($this->shippingMethods,1)."<pre>";?>