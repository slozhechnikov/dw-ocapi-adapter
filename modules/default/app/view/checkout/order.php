<?php
//echo "<pre>".print_r($this->order, 1)."</pre>";
/*
*/
?>
<div class="order-container">
	<div class="order-head">
		Order
	</div>
	<div class="product-items">
		<?php echo $this->partial("app/view/basket/partials/basketTable.php", array("basket"=>$this->order))?>
	</div>
	<div class="order-billing">
		<div class="customer-name">
			<?php echo $this->order["billing_address"]["full_name"]?>
		</div>
		<div class="customer-phone">
			<?php echo $this->order["billing_address"]["phone"]?>
		</div>
		<div class="customer-postal_code">
			<?php echo $this->order["billing_address"]["postal_code"]?>
		</div>
		<div class="customer-address">
			<?php echo $this->order["billing_address"]["address1"]?>
		</div>
	</div>
	<div class="order-shipping">
		<div class="shipping-name">
			<?php echo $this->order["shipments"][0]["shipping_method"]["name"]?>
		</div>
		<div class="shipping-description">
			<?php echo $this->order["shipments"][0]["shipping_method"]["description"]?>
		</div>
		<div class="shipping_price">
			<?php echo $this->order["shipments"][0]["shipping_method"]["price"]?>
		</div>
	</div>
	<div class="checkout-next">
		<a href="<?php echo urlService::get()->getUrl("checkout", "payment")?>">
			next
		</a>
	</div>
</div>