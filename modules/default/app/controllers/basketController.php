<?php
class basketController extends controllerAbstract {
	
	private $client;
	private $requestService;
	
	public function init() {
		$this->client = client::get();
		$this->requestService = requestService::get();
	}
	
	public function addAction() {
		$post = $this->requestService->postParams; 
		echo $this->client->addToBasket($post);
		die;
	}
	
	public function indexAction() {	
		$this->view->basketJSON = $_SESSION["basket"];
		$this->view->basket = $basket = json_decode($_SESSION["basket"], 1);		
		$products = array();
		foreach($basket["product_items"] as $item){
			$products[] = $item["product_id"];
		}
		$this->view->addToJs('basket');
		$this->view->products = $this->client->getProductsInfoById($products);
	}
	
	public function removeItemAction() {			
		$itemKey = $this->requestService->getParams["itemKey"];
		$result = $this->client->removeItem($itemKey);
		echo $result; die;
	}
	
	public function updateQtyAction() {
		$itemKey = $this->requestService->getParams["itemKey"];
		$qty = $this->requestService->getParams["qty"];
		$result = $this->client->updateItemQty($itemKey, $qty);
		echo $result; die;
	}
}
?>