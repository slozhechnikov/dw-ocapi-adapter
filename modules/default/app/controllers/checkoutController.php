<?php
class checkoutController extends controllerAbstract {
	private $client;		
	private $requestService;
	
	public function init() {
		$this->client = client::get();
		$this->requestService = requestService::get();
		$this->layout->put("checkout");
	}
	
	public function indexAction() {
		header("Location: http://demo.ocapi.demandware.net/");
	//!!do not delete, the time of this code will come!!
	/*
		$login = $this->client->login();
		if($login["fault"]) {
			$reg = $this->client->register();
			$login = $this->client->login();
		}	
	*/
		//$this->view->fbUserInfo = facebookService::get()->user;
		$this->view->shippingMethods = $this->client->getShippingMethods();
	}

	public function orderAction() {
		$this->view->order = $this->client->setCheckoutInfo($this->requestService->postParams);
	}
	
	public function paymentAction() {
		$this->view->paymentMethods = $this->client->getPaymentsMethods();
	}
	
	public function completeAction() {
		$this->layout->put("main");
		$this->client->setPaymentsMethods($this->requestService->postParams["payment_method"]);
		$this->view->order = $this->client->checkoutComplete();
		if($this->view->order)
			$this->client->clearBasket();
		try {
			facebookService::get()->post(array("message"=>"atata, I've bought something"));
		}
		catch(Exception $e) {}
	}
	
	public function DWOrderStatusExportTestAction() {
	//echo 1;die;
		$curl_resource = curl_init("http://development.reebok.adidasgroup.demandware.net/on/demandware.store/Sites-Reebok-CA-Site/en_CA/YourReebok-OrderStatusUpdate");
		//curl_setopt($curl_resource, CURLOPT_HTTPHEADER, array("multipart/form-data"));
		curl_setopt($curl_resource, CURLOPT_POST, 1);	
		//curl_setopt($curl_resource, CURLOPT_HEADER, true);		
		curl_setopt($curl_resource, CURLOPT_POSTFIELDS, 'orderStatusUpdate=<?xml version="1.0" encoding="UTF-8"?><orderStatusUpdate rbkCustomOrderId="CA6000073" trackingNumber="934835851631" ecommerceOrderId="CA6000073" status="SHIPPED"><trackingURL><![CDATA[http://fedex.com/Tracking?tracknumber_list=xxxxxx]]></trackingURL></orderStatusUpdate>');
		header('content-type: text/xml');
		curl_exec($curl_resource);
		die;
	}
	
	public function DWOrderExportTestAction() {
		print_r($_GET);
		die;
	}
}
?>