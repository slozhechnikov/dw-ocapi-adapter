<?php
class indexController extends controllerAbstract {

	private $client;
	private $requestService;
	
	public function init() {
		$this->client = client::get();
		$this->requestService = requestService::get();
	}
	
	private function getPageing($params) {
		ob_start();
			$this->view->partial("app/view/index/partials/paging.php", $params);
			$data = ob_get_contents();
		ob_end_clean();	
		return $data;
	}
	
	public function indexAction() {
        $params = array('page'=>1);
        $res = $this->client->searchProducts($params);
        $tools = $this->getModel("tools", "default");
        $res["products"]["refinements"] = $tools->arrayToHash($res["products"]["refinements"], array(
            "keyAttr"=>"attribute_id",
            "next"=>array(
                "subKey"=>"values",
                "keyAttr"=>"value",
                "skipAttr"=>"hit_count",
                "next" =>array(
                    "subKey"=>"values",
                    "skipAttr"=>"hit_count",
                    "keyAttr"=>"value"
                )
            )
        ));
        $this->view->searchResult = $res;

	}
	
	/*public function productListAction() {
		$this->requestService->getParams["page"] = $this->requestService->getParams["page"] ? $this->requestService->getParams["page"] : 1;
		$this->defineView("app/view/index/search.php");
		$this->view->searchResult = $res = $this->client->searchProducts($this->requestService->getParams);
		//print_r($this->view->searchResult);
		$this->view->addToJs('search');
		$this->view->pages = $this->getPageing(array("total"=>$res["products"]["total"], 'count'=>$res["products"]["count"], "start"=>$res["products"]["start"], "search"=>1));	 
	}*/
	
	public function productPageAction() {
		$this->view->addToJs("product");
		$this->view->productJSON = $this->client->getProductByID($this->requestService->getParams["pid"]);
		$res = json_decode($this->view->productJSON, 1);
		$tools = $this->getModel("tools", "default");
		$res["variation_attributes"] = $tools->arrayToHash($res["variation_attributes"], array("keyAttr"=>"id"));
		$this->view->product = $res;		
		$this->view->breadCrumbs = $tools->breadCrumbs($this->view->product["primary_category_id"], $this->client->getCategoryList("root", array("levels"=>2)));
		$this->view->YMALProducts = $this->client->searchProducts(array(
			"cgid"=>$this->view->product["primary_category_id"], 
			"count"=>2,
			"sort"=>"top-sellers"
		));
		//echo "<pre>".print_r($this->view->YMALProducts, 1)."</pre>";
	}
	
	/*private function humanSelectedRef($ref, $selRef) {
		function restructurRef($vr) {
			while($vv = current($vr["values"])) {					
				if($vv["hit_count"] == 0) {
					next($vr["values"]);
					continue;
				}
				if($selRef[$vr["attribute_id"]] == $vv["value"] || preg_match('/(?:^|\|)'.$vv["value"].'(?:\||$)/', $selRef[$vv["attribute_id"]])) {	
					if(gettype($res["products"]["selected_refinements"][$vr["attribute_id"]]) != "array") {		
						print_r($res["products"]["selected_refinements"][$vr["attribute_id"]]);
						//$res["products"]["selected_refinements"][$vr["attribute_id"]] = "23";	
									
						$res["products"]["selected_refinements"][$vr["attribute_id"]] = array(
							"lable"=>$vr["label"],
							"values"=>array($vv)
						);
						print_r($res["products"]["selected_refinements"]);	
					}
					else {
						array_push($res["products"]["selected_refinements"][$vr["attribute_id"]]["values"], $vv);
					}
				}
				else if($vv["values"]) {
					while($subvv = current($vv["values"])) {
						if($vv["hit_count"] == 0) {
							next($vv["values"]);
							continue;
						}
						next($vv["values"]);
					}
				}
				next($vr["values"]);
			}
		}
	}
	*/
	public function searchAction() {
		$params = !empty($this->requestService->postParams) ? $this->requestService->postParams : $this->requestService->getParams;
		$q = $params["q"];
		if(!$params["page"]) $params["page"] = 1;
		$this->view->format = $this->requestService->getParams["format"];	
		if($this->view->format == "ajax") {
			$this->layout->put("disable");
		}
		else {
			$this->view->addToJs('search');
		}
		$res = $this->client->searchProducts($params);	
		$tools = $this->getModel("tools", "default");
		//print_r($params);
		if($params["cgid"])
			$this->view->breadCrumbs = $tools->breadCrumbs($params["cgid"], $this->client->getCategoryList("root", array("levels"=>2)));		
		$res["refinements"] = $tools->arrayToHash($res["refinements"], array(
			"keyAttr"=>"attribute_id", 
			"next"=>array(
				"subKey"=>"values", 
				"keyAttr"=>"value",
				"skipAttr"=>"hit_count",
				"next" =>array(
					"subKey"=>"values", 
					"skipAttr"=>"hit_count",
					"keyAttr"=>"value"
				)
			)
		));
		$res["requested_count"] = $params["count"] ? $params["count"] : settings::get("DEFAULT_ITEMS_ON_PAGE");
		//echo "<pre>".print_r($res, 1)."<pre>";
		$this->view->searchResult = $res;
		$this->view->pages = $this->getPageing(array(
			"total"=>$res["total"], 
			'count'=>$res["count"], 
			"start"=>$res["start"], 
			"search"=>1, 
			"requested_count"=>$res["requested_count"]
		));	
	}
}
?>