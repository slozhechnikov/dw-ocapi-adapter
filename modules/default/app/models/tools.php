<?php
class tools {
	private static $instance;
	private function __construct() {
	}
	public static function get() {	
		if(is_null(self::$instance)) {
			self::$instance = new tools();
		}
		return self::$instance;
	}
	public function arrayToHash($arr, $attrs) {	
		function restruct($arr, $keyAttr ,$skipAttr, $next) {
			$resultArr = array();
			while($current = current($arr)) {
				if($skipAttr && $current[$skipAttr] == 0){
					next($arr);
					continue;
				}
				$resultArr[$current[$keyAttr]] = $current;
				if($next && $resultArr[$current[$keyAttr]][$next["subKey"]]) {
					$resultArr[$current[$keyAttr]][$next["subKey"]] = restruct($resultArr[$current[$keyAttr]][$next["subKey"]], $next["keyAttr"], $next["skipAttr"],  $next["next"]);
				}
				next($arr);
			}
			return $resultArr;
		}
		return restruct($arr, $attrs["keyAttr"], $attrs["skipAttr"], $attrs["next"]);
	}
	
	public function breadCrumbs($category, $categories) {	
		$categories = $categories["categories"];
		while($currentCategory = current($categories)) {
			$foo = $currentCategory["categories"];
			while($subCat = current($foo)) {
				if(strrpos($category, $subCat["id"]) !== false) {
					return array($currentCategory, $subCat);
				}
				next($foo);				
			}
			next($categories);
		}
		return false;
	}
}
?>