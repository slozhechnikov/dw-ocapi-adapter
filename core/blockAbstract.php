<?php
abstract class blockAbstract {

    public $view;

    public function __construct($name) {
        $this->view = new view("blocks/templates/" . $name . ".php");
    }

    public function show() {
        $this->get();
        $this->view->show();
    }
    public function get() {}
	
	protected function getModel($modelName, $model) {		
		include_once(filePathService::get()->getPath("app/models/".$modelName.".php", $model));
		return $modelName::get();
	}
}
