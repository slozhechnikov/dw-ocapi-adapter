<?php
require_once "core/services/facebook/facebook.php";
require_once "core/services/facebook/AppInfo.php";
require_once "core/services/facebook/utils.php";
$fb = new Facebook(array(
			  'appId'  => AppInfo::appID(),
			  'secret' => AppInfo::appSecret(),
			));
class facebookService  extends Facebook {
	
	private static $instance;
	
	public $user;
	
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new facebookService(array(
			  'appId'  => AppInfo::appID(),
			  'secret' => AppInfo::appSecret(),
			));
			self::$instance->user = self::$instance->getUserInfo();
		}
		return self::$instance;
	}
	
	public function getUserInfo($fields) {
		return $this->api('/me');
	}
	
	public function post($params) {	
		try {
			$this->api("/me/feed", "post", $params);
		}
		catch(Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}
?>