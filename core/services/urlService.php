<?php
class urlService {
	
	private $clientName;
	
	private static $instance;
	
	private function __construct() {
		global $CURRENT_CLIENT;
		$this->clientName = $CURRENT_CLIENT["client_name"];
	}
	
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new urlService();
		}
		return self::$instance;
	}
	
	public function getUrl($controller = "index", $action = "index", $params = null) {
		return "/".$this->clientName."/".$controller."/".$action.($params ? "/?".http_build_query($params) : "/");
	}
	
	public function redirect($controller = "index", $action = "index", $params) {
		header("location:".$this->getUrl);
	}
	
	public function getFullUrl($controller, $action, $params) {
		return $_SERVER["HTTP_X_FORWARDED_PROTO"]."://".$_SERVER["HTTP_HOST"].$this->getUrl($controller, $action, $params);
	}
}
