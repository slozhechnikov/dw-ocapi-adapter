<?php
class requestService  {
	
	public $getParams = array();
	
	public $postParams = array();
	
	private static $instance;
	
	private function __construct() {
		foreach($_GET as $k=>$v) {
			$this->getParams[$k] = $v;
		}
		foreach($_POST as $k=>$v) {
			$this->postParams[$k] = $v;
		}
	}
	
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new requestService();
		}
		return self::$instance;
	}
}
?>