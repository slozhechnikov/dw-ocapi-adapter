<?php 
class filePathService {
	
	private $client;
	
	private static $instance;
	
	private function __construct() {
		global $CURRENT_CLIENT;
		$this->client = $CURRENT_CLIENT;
	}
	
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new filePathService();
		}
		return self::$instance;
	}
	
	public function getPath($path, $module = 'default') {
		if(isset($module))
			return("modules/".$module."/".$path);
		while($module = current($this->client["modules"])) {
			if(file_exists("modules/".$module."/".$path))
				return "modules/".$module."/".$path;
			next($this->client["modules"]);
		}
		return false;
	}
}

