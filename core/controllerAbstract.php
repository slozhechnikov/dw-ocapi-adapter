<?php
abstract class controllerAbstract {
	public $view;
	protected $layout;
	
	public function __construct($actionName, $controllerName){
		$this->view = $this->defineView("app/view/".$controllerName."/".$actionName.".php");		
		$this->layout = new layout();		
		$this->init();
		$methodName = $actionName."Action";
		$this->$methodName();
		$this->output();
	}
	
	public function __call($name, $arguments) {
		if($name != "init") {		
			$this->defineView("app/view/error.php");
			$this->view->errorMessage = "page not found";
		} 
	}	
	
	protected function defineView($fileName) {
		if(is_null($this->view)) {
			$this->view = new view($fileName);
		}
		else {
			$this->view->changeFile($fileName);
		}		
		return $this->view;
	}	
	
	private function output() {		
		$this->layout->get($this->view);		
	}	
	
	protected function getModel($modelName, $model) {		
		include_once(filePathService::get()->getPath("app/models/".$modelName.".php", $model));
		return $modelName::get();
	}
}
?>