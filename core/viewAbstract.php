<?php
class viewAbstract {
	private $file;
	private $params;
	private $filePathService;
	private $js = array();
	 
	public function __construct($fileName) {	
		$this->file = $fileName;
		$this->filePathService = filePathService::get();
	}
	
	public function changeFile($fileName) {
		$this->file = $fileName;
	}
	
	public function show() {
		include $this->filePathService->getPath($this->file);
	}
	
	public function partial($fileName, $params = null) {
		$part = new view($fileName);
		$part->params = $params;
		$part->show();
	}
	
	public function addToJs($files) {
		if(is_array($files)) {
			foreach($files as $file) {
				$this->js[] = $file;
			}
		}
		else {
			$this->js[] = $files;
		}
	}
	
	public function includeJS() {
		if(!empty($this->js)) {
			foreach($this->js as $file){
				echo '<script src="/'.$this->filePathService->getPath('static/js/'.$file.'.js').'"></script>';
			}
		}
	}
	
	public function __get($name) {
		return !is_null($this->params[$name]) ? $this->params[$name] : "";
	}
	
	public function __set($name, $value) {
		$this->params[$name] = $value;
	}
}
?>