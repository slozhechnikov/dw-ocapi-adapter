<?php
abstract class clientAbstract {
	
	public $site;
	public $client_id;
	
	protected $cache;		
		
	protected function __construct() {
		global $CURRENT_CLIENT;	
		$this->site = $CURRENT_CLIENT["site"];
		$this->client_id = $CURRENT_CLIENT["client_id"];	
		$this->cache = array();
	}
	
	protected function putInCache($key, $value) {
		$this->cache[$key] = $value;
		return $value;
	}
	
	protected function getFromCache($key) {
		return $this->cache[$key];
	}
	
	private function callUrl($url, $json) {		
		return $json ? file_get_contents($url) : json_decode(file_get_contents($url), 1);
	}
	
	protected function getUrl($method, $params) {
	//echo "https://demo.ocapi.demandware.net/s/".$this->site."/dw/shop/v13_1/".$method."?client_id=".$this->client_id.($params ? "&".http_build_query($params) : "");
		return "https://demo.ocapi.demandware.net/s/".$this->site."/dw/shop/v13_1/".$method."?client_id=".$this->client_id.($params ? "&".http_build_query($params) : "");		
	}

	protected function clientQuery($method, $params = array(), $json = null) {
		return $this->callUrl($this->getUrl($method, $params), $json);		
	}
	
	protected function curlQuery($method, $get=array(), $post=null, $patch=null) {
		//$url = $this->getUrl($method, $get);
		$curl_resource = curl_init($this->getUrl($method, $get));
		curl_setopt($curl_resource, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		if($_SESSION["ETag"]) {	
			curl_setopt($curl_resource, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "If-Match: ".$_SESSION["ETag"]));			
		}
		if($patch)
			curl_setopt($curl_resource, CURLOPT_CUSTOMREQUEST, "PATCH" );
		if($post) {			
			if($patch) {
				curl_setopt($curl_resource, CURLOPT_PATCH, 1);
			}
			else
				curl_setopt($curl_resource, CURLOPT_POST, 1);
			curl_setopt($curl_resource, CURLOPT_POSTFIELDS, $this->DWJSON_encode($post));
		}
		curl_setopt($curl_resource, CURLOPT_HEADER, true);
		if($_SESSION["bc"]) {
			curl_setopt($curl_resource, CURLOPT_COOKIE, $_SESSION["bc"]);
		}
		curl_setopt($curl_resource, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl_resource, CURLOPT_COOKIESESSION, TRUE); 				
		ob_start();
			curl_exec($curl_resource);
			$data = ob_get_contents();
		ob_end_clean();	
		$data = array(
			"header" => substr($data,0,curl_getinfo($curl_resource, CURLINFO_HEADER_SIZE)),
			"body" => substr($data,curl_getinfo($curl_resource, CURLINFO_HEADER_SIZE))
		);
		curl_close($curl_resource);	
		preg_match('/ETag: (\S+)/', $data["header"], $m);
		if($m[1])
			$_SESSION["ETag"] = $m[1];
		return $data;	
	}
	
	public function DWJSON_encode($array) {
		return preg_replace('/\"(\d+\.\d{2})\"/', '$1', json_encode($array));
	}
}
?>