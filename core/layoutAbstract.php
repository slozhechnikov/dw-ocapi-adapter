<?php
abstract class layoutAbstract {

	public $view;			
	
	private $filePathService;
	
	public function __construct() {
		$this->filePathService = filePathService::get();
	}

    public function includeJS() {
		foreach($this->js[$this->file_name] as $jsName) {
			echo '<script src="/'.$this->filePathService->getPath('static/js/'.$jsName.'.js').'"></script>';
		}
	}
	
	public function put($file_name) {
		$this->file_name = $file_name;
	}
	
	public function get($view) {	
		$this->view = $view;			
		if($this->file_name == "disable") {			
			$this->view->show();			
			return false;
		}
		foreach($this->allBlocksName[$this->file_name] as $blockName) {
			include_once($this->filePathService->getPath("blocks/".$blockName.".php"));
			$blockClass = $blockName."Block";
			$this->blocks[$blockName] = new $blockClass($blockName);
		}
		$path = $this->filePathService->getPath("layouts/".$this->file_name.".php");
		include($path);
	}
}
?>