<?php
class client extends clientAbstract {
		
	private static $instance;
		
	public static function get() {
		if(is_null(self::$instance)) {
			self::$instance = new client();
		}
		return self::$instance;
	}
	
	public function getClientId() {
		return $this->client_id;
	}
	
	
	public function getCategoryList($category_id, $params) {		
		if($this->getFromCache("categoryRoot")) return $this->getFromCache("categoryRoot");		
		return $this->putInCache("categoryRoot", $this->clientQuery("categories/".$category_id, $params));			
	}
	
	public function getProductsByCID($CID, $page) {
		$start = ($page-1) * settings::get("DEFAULT_ITEMS_ON_PAGE");
		$result = $this->clientQuery("product_search/images/", array("refine_1"=>"cgid=".$CID, "start"=>$start, "count"=>settings::get("DEFAULT_ITEMS_ON_PAGE")));
		return $result;
	}
	
	public function searchProducts($post = array()) {
		$i = 0;
		foreach($post as $k=>$v) {
			$i++;
			switch($k) {
				case "q":
				case "sort":
				case "count":
					$params[$k] = $v; 
				break;
				case "page" :
				break;
				default : $params["refine_".$i] = $k."=".$v;
			}
		}			
		if(!$params["count"]) $params["count"] = settings::get("DEFAULT_ITEMS_ON_PAGE");
		$params["start"] = ($post["page"]-1) * $params["count"];
		if($params["start"] < 0) $params["start"] = rand(1, 3);
		$params["expand"] = "prices,images";
		$result = $this->clientQuery("product_search", $params);
		//echo "<pre>".print_r($params, 1)."</pre>"; 
		return $result;
	}
	
	public function getProductByID($PID) {
		$result = $this->clientQuery("products/".$PID, array("expand"=>"availability,bundled_products,links,promotions,options,images,prices,variations,set_products"), 1);
		return $result;
	}
	
	public function addToBasket($post) {
		$data = $this->curlQuery("basket/this/add", null, array("product_id"=>$post["product_id"], "quantity"=>$post["quantity"]));
		$_SESSION["basket"] = $data["body"];			
		if(is_null($_SESSION["bc"])){									
			preg_match_all("/Set-Cookie: (.*?)=(.*?);/i", $data["header"], $res);
			$cookie = '';
			while(list($key, $value) = each($res[1])) {
				$cookie .= $value.'='.$res[2][$key].'; ';				
			};
			$_SESSION["bc"] = $cookie;				
		}		
		$body = $data["body"];		
		if($post["options"]) {		
			$basket_array = json_decode($data["body"], 1);	
			$opt_array = array();
			foreach($post["options"] as $key=>$val){
				$opt_array[] = array("_at"=>$key, "option_value_id"=>$val);
			}		
			$params = array(
				"product_items"=>array(
					array(
						"_at" => count($basket_array["product_items"])-1,
						"option_items" => $opt_array
					)
				)
			);		
			$this->updateBasket($params);
		}							
		return $_SESSION["basket"];
	}
	
	public function getProductsInfoById($products, $params) {		
		$products_str = urlencode(implode("+", $products));	
		return $this->clientQuery("products/(".$products_str.")", $params);
	}
	
	public function updateBasket($upd) {
		$data = $this->curlQuery("basket/this", null, $upd, 1);
		if(!json_decode($data["body"])) return json_encode(array("error"=>1, "message"=>lang::get()->getVal("update_basket_error")));
		$_SESSION["basket"] = $data["body"];
		return $_SESSION["basket"];
	}
		
	public function removeItem($itemKey) {
		$params = array(
			"product_items"=>array(
				array("_delete_at"=>(int)$itemKey)
			)
		);				
		return $this->updateBasket($params);
	}
	
	public function updateItemQty($itemKey, $qty) {
		$params = array(
			"product_items"=>array(
				array(
					"_at"=>(int) $itemKey,
					"quantity" => $qty.".00"
				)
			)
		);
		return $this->updateBasket($params);
	}
	
	public function getShippingMethods() {
		return $this->clientQuery('basket/this/checkout/shipping_methods');
	}
	
	public function setCheckoutInfo($post) {
		$custometInfo = $this->curlQuery("basket/this/checkout/set_customer_info", null, array("email"=>$post["email"]));
		$adress = array(
			"salutation"=>"",
			"title"=>"",
			"company_name"=>"",
			"first_name"=>$post["first_name"],
			"second_name"=>"",
			"last_name"=>$post["last_name"],
			"postal_code"=>$post["postal_code"],
			"address1"=>$post["street"]." ".$post["house_number"],
			"address2"=>$post["add_adress"],
			"city"=>$post["city"],
			"post_box"=>"",
			"country_code"=>"",
			"state_code"=>"",
			"phone"=>$post["phone"],
			"suffix"=>""
		);
		$shippingAdress = $this->curlQuery("basket/this/checkout/set_shipping_address", null, $adress);
		$shippingBilling = $this->curlQuery("basket/this/checkout/set_billing_address", null, $adress);
		$shippingMethod = $this->curlQuery("basket/this/checkout/set_shipping_method", null, array("id"=>$post["shipping_method"]));
		return json_decode($shippingMethod["body"], 1);
	}
	
	public function getPaymentsMethods() {		
		return $this->clientQuery("basket/this/checkout/payment_methods");
	}
	
	public function setPaymentsMethods($id) { 
		$res = $this->curlQuery("basket/this/checkout/set_payment_method", null, array("id"=>$id));
		return json_decode($res["body"], 1);
	}	
	
	public function checkoutComplete() {
		return $this->clientQuery("basket/this/checkout");
	}
	
	public function clearBasket() {
		$basket = json_decode($_SESSION["basket"], 1);
		$productCount = count($basket["product_items"]);
		$toDel = array();
		for($i=0; $i<$productCount; $i++) {
			$toDel[] = array("_delete_at"=>$i);
		}
		$params = array("product_items"=>$toDel);
		return $this->updateBasket($params);
	}
	
	public function login() {
		$facebook = facebookService::get();
		$user = $facebook->getUserInfo();
		$password = md5($facebook->getAccessToken());	
		$res = $this->curlQuery("account/login", null, array("username"=>$user["email"], "password"=>$password));
		return json_decode($res["body"], 1);
	}
	
	public function register() {
		$facebook = facebookService::get();
		$user = $facebook->getUserInfo();
		$password = md5($facebook->getAccessToken());
		$date = new DateTime($user["birthday"]);
		$params = array(
			"credentials"=>array(
				"username"=>$user["email"],
				"password"=>$password
			),
			"profile"=>array(
				"email"=>$user["email"],
				"birthday"=>date_format($date, 'Y-m-d'),
				"fax"=>"",
				"first_name"=>$user["first_name"],
				"gender"=>"m",
				"job_title"=>"",
				"last_name"=>$user["last_name"],
				"phone_business"=>"",
				"phone_home"=>"",
				"phone_mobile"=>"",
				"preferred_locale"=>$user["locale"],
				"salutation"=>"",
				"second_name"=>"",
				"suffix"=>"",
				"title"=>""
			)
		);

		echo "<pre>".print_r($params, 1),"</pre>";
		$res = $this->curlQuery("account/register", null, $params);
		return json_decode($res["body"], 1);
	}
	
}
?>