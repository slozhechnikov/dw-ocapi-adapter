<?php
date_default_timezone_set('America/Los_Angeles');
//$date_array = array("start"=>date("i:s:u"));
require_once('core/clientAbstract.php');

//parse url and routing
if(isset($_SERVER["REDIRECT_URL"])) {
	$m = explode("/", $_SERVER["REDIRECT_URL"]);
}
else{
	$m = array("1", "test", "index", "index");
}

$clientName = isset($m[1]) ? $m[1] : "test";
$controllerName = isset($m[2]) ? $m[2]: "index";
$actionName  = isset($m[3]) ? $m[3] : "index";

if(!isset($m[1]))
	header("location:/test/");
//$date_array["parse url"] = date("i:s:u");
session_start();

require_once("clients.php");
$CURRENT_CLIENT = $clients[$m[1]];
require_once('models/'.$CURRENT_CLIENT["client_model"].'.php');
//load services
$serviceDir = opendir("core/services");
while($service = readdir($serviceDir)) {
	if(preg_match('/\.php$/', $service)) 
		require_once("core/services/".$service);
}
closedir($serviceDir);
//print_r($_SESSION);
require_once('core/viewAbstract.php');
require_once('core/blockAbstract.php');
require_once('core/controllerAbstract.php');
require_once('core/layoutAbstract.php');

//$date_array["load abstract"] = date("i:s:u");
require_once(filePathService::get()->getPath("core/view.php"));
require_once(filePathService::get()->getPath("core/layout.php"));
require_once(filePathService::get()->getPath("resources/settings.php"));
require_once(filePathService::get()->getPath("resources/lang.php"));
//load models
/*$modelDir = opendir("app/models");
while($model = readdir($modelDir)) {
	if(preg_match('/\.php$/', $model)) 
		require_once("app/models/".$model);
}
closedir($modelDir);*/

//run current controller
//facebookService::get(); 
require_once(filePathService::get()->getPath("app/controllers/".$controllerName."Controller.php"));
$className = $controllerName."Controller";
$controller = new $className($actionName, $controllerName);
//$date_array["end"] = date("i:s:u");
//echo "<pre>".print_r($date_array, 1)."</pre>";

